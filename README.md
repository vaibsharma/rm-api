# RM API

The project is started to help colleges that don't have RM(Resume managers) for the companies that visit on campus. 


# Installation

* MySQL - version 8.0.17, you can download them from here - https://dev.mysql.com/downloads/mysql/ . Also for tutorial on how to install it on ubuntu please have a look at this [documentation](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04)
* Clone the repo `git clone https://gitlab.com/vaibsharma/rm-api.git`
* `cd rm-api`
* Install virtualenv by `sudo pip install virtualenv` and create a virtualenv with python3 by `virtualenv -p python3 env`
* pip install -r requirements.txt
* python manage.py runserver

Trouble shooting guide with Mysql

To create an issue please add [here](https://gitlab.com/vaibsharma/rm-api/-/issues)
Make sure that issue description includes:
1. Preproducing steps.
2. Screenshots/recordings.

To request for any feature add [here](https://gitlab.com/vaibsharma/rm-api/-/issues) with the `FEATURE` prefix as title.

Feel free to make a PR for any issue fix and features. Would be really helpful :)


If you are having any trouble installing mysqlclient, try this once:
LDFLAGS=-L/usr/local/opt/openssl/lib pip install mysqlclient
