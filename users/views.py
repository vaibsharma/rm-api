from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User
from rest_framework.views import APIView
from .serializers import UserSerializer
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from django.http import JsonResponse
from django.contrib.auth.models import AnonymousUser
from utils.exceptions import AnonymousUserError


class UserProfile(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *arg, **kwargs):
        username = kwargs.get('username', None)
        user = request.user
        if user.is_anonymous:
            raise AnonymousUserError("Error on getting user, anonymous request")
        if username is not None:
            user = User.objects.get(username=username)
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data, safe=False)





