from django.conf.urls import url, include
from .views import UserProfile

app_name = 'user'

url_patters = [
    url(r'^profile/((?P<username>[a-zA-Z0-9_]+)/)?$', UserProfile.as_view(), name="profile"),
]


