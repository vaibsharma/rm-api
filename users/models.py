from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from base_model import RMBase
from utils.exceptions import ModelSaveError
from utils.commons import phone_regex


class UserProfile(RMBase):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    mobile = models.CharField(validators=[phone_regex], max_length=17, null=True, blank=True)
    bio = models.CharField(max_length=132, default='', blank=True)
    avatar = models.ImageField(upload_to='avatar/', blank=True)
    is_recruiter = models.BooleanField(default=False)
    is_candidate = models.BooleanField(default=False)
    is_interviewer = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        count = 0
        if self.is_interviewer:
            count = count + 1
        if self.is_candidate:
            count = count + 1
        if self.is_recruiter:
            count = count + 1
        if count > 1:
            raise ModelSaveError("UserProfile can only either be a student, an interviewer or a recruiter")
        return super(UserProfile, self).save(*args, **kwargs)

    @property
    def type(self):
        if self.is_interviewer:
            return "interviewer"
        if self.is_candidate:
            return "candidate"
        if self.is_recruiter:
            return "recruiter"
        # return "student"
        raise Exception('Userprofile has to be either a student, an interviewer or a recruiter')

    def __str__(self):
        return self.user.username + " " + self.type
