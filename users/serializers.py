from django.contrib.auth.models import User
from .models import UserProfile
from rest_framework import serializers
from django.utils.timezone import now


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ['mobile', 'bio', 'avatar', 'type']
        # fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(read_only=True)
    data = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'profile', 'data']
        # fields = '__all__'

    def get_data(self, obj):
        from company.serializers import RecruiterSerializer, InterviewerSerializer
        from university.serializers import CandidateSerializer
        if obj.profile.is_recruiter:
            recruiter = obj.recruiter
            serializer = RecruiterSerializer(recruiter)
        if obj.profile.is_candidate:
            candidate = obj.candidate
            serializer = CandidateSerializer(candidate)
        if obj.profile.is_interviewer:
            interviewer = obj.interviewer
            serializer = InterviewerSerializer(interviewer)
        return serializer.data

