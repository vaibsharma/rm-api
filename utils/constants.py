class DBVariables:
    DB_NAME = "resume_manager"
    DB_USER = "root"
    DB_PASSWORD = ""

# environments


PRODUCTION = "production"
DEVELOPMENT = "development"
TESTING = "staging"

# Variables


NONE = "None"


class AcademicProgramChoices(object):
    BTECH = "BTech"
    BE = "BE"
    BSC = "BSc"
    BBA = "BBA"
    BCOM = "BCOM"
    MTECH = "MTECH"
    MS = "MS"
    PHD = "PHD"

    choices = (
        (BTECH, "B.Tech"),
        (BE, "B.E"),
        (BSC, "B.Sc"),
        (BBA, "B.B.A"),
        (BCOM, "B.Com"),
        (MTECH, "M.Tech"),
        (MS, "MS"),
        (PHD, "Ph.D"),
    )

