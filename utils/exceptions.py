from rest_framework.exceptions import APIException


class RMBaseException(Exception):
    def __init__(self, message, error=None):
        self.errors = error
        return super(RMBaseException, self).__init__(message)


class RMBaseAPIException(APIException):
    pass


class ModelSaveError(RMBaseException):
    def __init__(self, message):
        super(ModelSaveError, self).__init__(message)
        self.error = "Model Save Error"


class AnonymousUserError(RMBaseAPIException):
    def __init__(self, message):
        super(AnonymousUserError, self).__init__(message)
        self.error = "Anonymous User"


class PostSubmitError(RMBaseException):
    def __init__(self, message):
        super(PostSubmitError, self).__init__(message)
        self.error = "Post can't be published"