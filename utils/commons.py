import datetime
from rest_framework.permissions import BasePermission
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


phone_regex = RegexValidator(
    regex=r'^\+?1?\d{9,15}$',
    message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
)


def year_choices():
    return [(r, r) for r in range(1984, datetime.date.today().year+1)]


def current_year():
    return datetime.date.today().year


def year_validator(value):
    try:
        year = int(value)
        if year < (current_year() - 5):
            raise ValidationError("The batch year cannot be 5 years less than the current year")
        if year > current_year():
            raise ValidationError("Cannot set batch for next years")
    except Exception as e:
        raise e


class RMException(Exception):
    def __init__(self, message, error=None):
        self.errors = error
        return super(RMException, self).__init__(message)


class IsCandidate(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        try:
            candidate = user.candidate
            return True
        except Exception as e:
            pass
        return False


class IsRecruiter(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        try:
            recruiter = user.recruiter
            return True
        except Exception as e:
            pass
        return False


class IsInterviewer(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        try:
            interviewer = user.recruiter
            return True
        except Exception as e:
            pass
        return False


class IsInterviewerOrRecruiter(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        request.META['COMPANY'] = None
        if user.profile.is_recruiter or user.profile.is_interviewer:
            if user.profile.is_recruiter:
                request.META['COMPANY'] = user.recruiter.company
            else:
                request.META['COMPANY'] = user.interviewer.company
            return True
        return False


