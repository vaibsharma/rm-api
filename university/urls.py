from django.conf.urls import url, include
import oauth2_provider.views as oauth2_views
from .views import HomeFeed, UserApplication, OpeningsFeed, UpdateStatus
from django.conf import settings

# OAuth2 provider endpoints

app_name = "university"

urlpatterns = [
    url(r'^user_feeds/$', HomeFeed.as_view(), name='home_feed'),
    url(r'^applications/$', UserApplication.as_view(), name='user_application'),
    url(r'^application/update/status/', UpdateStatus.as_view(), name='update_status'),
    url(r'^openings/$', OpeningsFeed.as_view(), name='opening_feed'),
]