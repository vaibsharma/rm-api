from django.contrib import admin

# Register your models here.
from .models import Candidate, University, Course, AcademicProgram, Application, Post


@admin.register(Candidate)
class CandidateAdmin(admin.ModelAdmin):
    pass


@admin.register(University)
class UniversityAdmin(admin.ModelAdmin):
    pass


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    pass


@admin.register(AcademicProgram)
class AcademicProgramAdmin(admin.ModelAdmin):
    pass


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass
# admin.site.register(Candidate)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass
