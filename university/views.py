from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from users.serializers import UserSerializer
from company.serializers import CompanySerializer
from rest_framework import status
from django.http import JsonResponse
from company.models import Company
from .utils import get_student_user_feed, get_recruiter_or_interviewer_user_feed, get_openings_from_company, get_openings_in_university
from .models import Application
from company.models import Opening
from .serializers import ApplicationSerializer
from utils.commons import IsInterviewerOrRecruiter

class ListFeed(APIView):
    permission_classes = [IsAuthenticated]


class HomeFeed(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *arg, **kwargs):
        user = request.user
        if user.profile.is_candidate:
            return get_student_user_feed(user)
        if user.profile.is_recruiter or user.profile.is_interviewer:
            return get_recruiter_or_interviewer_user_feed(user)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class OpeningsFeed(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        if user.profile.is_candidate:
            return get_openings_in_university(user.candidate.university)
        if user.profile.is_recruiter:
            return get_openings_from_company(user.recruiter.company)
        if user.profile.is_interviewer:
            return get_openings_from_company(user.interviewer.company)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class UserApplication(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *arg, **kwargs):
        user = request.user
        serializer = None
        if user.profile.is_candidate:
            applications = Application.objects.filter(candidate=request.user.candidate).order_by('-created_at')
            serializer = ApplicationSerializer(applications, many=True)
        if user.profile.is_interviewer or user.profile.is_recruiter:
            try:
                company = user.recruiter.company
            except Exception as e:
                company = user.interviewer.company
            opening_ids = Opening.objects.filter(company=company).values_list('id', flat=True)
            print(opening_ids)
            applications = Application.objects.filter(opening__in=opening_ids)
            print(applications)
            serializer = ApplicationSerializer(applications, many=True)

        if serializer is not None:
            return JsonResponse(serializer.data, safe=False, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class UpdateStatus(APIView):
    permission_classes = [IsInterviewerOrRecruiter]

    def post(self, request):
        data = request.data;
        application_id = data.get('application', '')
        application_status = int(data.get('status', ''))
        if application_id == '' or status == '':
            return Response({'error': 'data is missing'}, status=status.HTTP_400_BAD_REQUEST)

        application = Application.objects.get(id=application_id)
        application.status = application_status
        application.save(update_fields=['status'])
        return Response({'status': 'status is updated'}, status=status.HTTP_200_OK)

