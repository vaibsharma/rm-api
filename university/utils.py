from university.models import Post, Candidate
from university.serializers import PostSerializer
from company.models import Opening, Recruiter
from company.serializers import OpeningSerializer
from django.http import JsonResponse
from django.db.models import Count, Q


def get_response_data(data_type, serializer_data):
    response = []
    for x in serializer_data:
        data = dict({})
        data["type"] = data_type
        data["data"] = x
        response.append(data)
    return response


def get_student_user_feed(user):
    university = user.candidate.university
    posts = Post.objects.filter(Q(university=university) | Q(author__is_staff=True)).order_by('-created_at')
    post_serializer = PostSerializer(posts, many=True)
    response = list([])
    response += get_response_data("post", post_serializer.data)
    return JsonResponse(response,  safe=False)


def get_recruiter_or_interviewer_user_feed(user):
    try:
        company = user.recruiter.company
    except Exception as e:
        company = user.interviewer.company
    recruiter = Recruiter.objects.filter(company=company).values_list('user_id')
    posts = Post.objects.filter(Q(author__in=recruiter) | Q(author__is_staff=True)).order_by('-created_at')
    post_serializer = PostSerializer(posts, many=True)
    response = list([])
    response += get_response_data("post", post_serializer.data)
    return JsonResponse(response, safe=False)


def get_openings_in_university(university):
    openings = Opening.objects.filter(university=university).order_by('-created_at')
    opening_serializer = OpeningSerializer(openings, many=True)
    response = get_response_data("opening", opening_serializer.data)
    return JsonResponse(response, safe=False)


def get_openings_from_company(company):
    openings = Opening.objects.filter(company=company).order_by('-created_at')
    opening_serializer = OpeningSerializer(openings, many=True)
    response = get_response_data("opening", opening_serializer.data)
    return JsonResponse(response, safe=False)


