from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator, MinLengthValidator
from utils.exceptions import ModelSaveError
from django.db.models import Count, Q
# from resume_manager.models import BaseModel
# from base_models import BaseModel
# from resume_manager import BaseModel

# Create your models here.


from base_model.models import RMBase
from company.models import Recruiter
from utils.commons import year_choices, year_validator, phone_regex
from utils.constants import AcademicProgramChoices
# from company.models import Opening


class University(RMBase):
    name = models.CharField(max_length=64, unique=True, null=False, blank=False)
    aka = models.CharField(max_length=10, null=False, blank=False)

    class Meta:
        verbose_name = 'University'
        verbose_name_plural = 'Universities'

    def __str__(self):
        return self.name


class AcademicProgram(RMBase):
    name = models.CharField(
        max_length=32,
        choices=AcademicProgramChoices.choices,
        default=AcademicProgramChoices.BTECH, null=False, blank=False
    )
    university = models.ForeignKey(University, related_name='program', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['name', 'university']

    def __str__(self):
        return "%s - %s" % (self.name, self.university)


class Course(RMBase):
    name = models.CharField(max_length=64, blank=False, null=False)
    program = models.ForeignKey(AcademicProgram, related_name='course', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ['name', 'program']

    def __str__(self):
        return "%s %s" % (self.name, self.program)


class Candidate(RMBase):
    user = models.OneToOneField(User, related_name='candidate', on_delete=models.CASCADE)
    batch = models.IntegerField(validators=[year_validator], null=False)
    resume = models.FileField(upload_to='resume/', blank=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    grade = models.FloatField(validators=[MinValueValidator(1), MaxValueValidator(100)], null=True)
    roll_no = models.CharField(max_length=16, blank=True)
    has_back = models.BooleanField(default=False)

    @property
    def university(self):
        university = University.objects.get(program=self.course.program)
        return university

    def __str__(self):
        return "%s" % self.user.username


class Application(RMBase):

    class Status(models.IntegerChoices):
        REJECT = 1
        WAITING = 2
        ON_GOING = 3
        COMPLETED = 4
        OFFER = 5

    opening = models.ForeignKey('company.Opening', on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    applied_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=Status.choices, null=False, default=Status.WAITING)

    class Meta:
        unique_together = ['opening', 'candidate']

    def __str__(self):
        return "%s - %s" % (self.candidate.user.first_name, self.opening.company)


class Post(RMBase):
    topic_name = models.CharField(max_length=32, blank=True, default='')
    message = models.TextField(max_length=1024*10, validators=[MinLengthValidator(32)])
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    university = models.ForeignKey('university.university', related_name='posts', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    likes = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def save(self, *arg, **kwargs):
        if not self.author.is_staff and not self.author.profile.is_recruiter:
            raise ModelSaveError('User should be an admin or recruiter')
        super(Post, self).save(*arg, **kwargs)

    @property
    def author_type(self):
        if self.author.is_staff:
            return self.author, "Admin"
        try:
            recruiter = Recruiter.objects.get(user=self.author)
            return self.author, recruiter.company
        except Exception as e:
            raise

    def add_likes(self):
        self.likes = self.likes + 1
        self.save(update_fields=['likes'])

    def __str__(self):
        return "%s - %s" % (self.topic_name, self.author.first_name)
