from django.utils.timezone import now

from rest_framework import serializers
from .models import University, Candidate, Application, AcademicProgram, Course, Post
from users.serializers import UserSerializer


class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = ['id', 'aka', 'name']


class AcademicProgramSerializer(serializers.ModelSerializer):
    university = UniversitySerializer(read_only=True)

    class Meta:
        model = AcademicProgram
        fields = ['id', 'name', 'university']


class CourseSerializer(serializers.ModelSerializer):
    program = AcademicProgramSerializer(read_only=True)

    class Meta:
        model = Course
        fields = ['id', 'name', 'program', 'is_active']


class CandidateSerializer(serializers.ModelSerializer):
    course = CourseSerializer(read_only=True)

    class Meta:
        model = Candidate
        fields = ['id', 'batch', 'resume', 'course', 'grade', 'has_back']


class ApplicationSerializer(serializers.ModelSerializer):
    from company.serializers import OpeningSerializer, InterviewSerializer
    opening = OpeningSerializer(read_only=True)
    interviews = InterviewSerializer(many=True, read_only=True)
    candidate = serializers.SerializerMethodField()

    class Meta:
        model = Application
        fields = ['id', 'opening', 'status', 'applied_at', 'interviews', 'candidate']

    def get_candidate(self, obj):
        serializer = UserSerializer(obj.candidate.user, read_only=True)
        return serializer.data


class PostSerializer(serializers.ModelSerializer):
    from users.serializers import UserSerializer
    university = UniversitySerializer(read_only=True)
    author = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'topic_name', 'university', 'author', 'message', 'likes', 'created_at']

