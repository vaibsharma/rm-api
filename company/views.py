from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from company.serializers import CompanySerializer, InterviewerSerializer
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework import status
from company.models import Company, Interview, Opening, Interviewer
from company.serializers import InterviewSerializer
from university.models import Application
from utils.commons import IsInterviewerOrRecruiter
from .actions import InterviewActions


class Companies(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        companies = Company.objects.all()
        serializer = CompanySerializer(companies, many=True)
        return JsonResponse(serializer.data, safe=False)


class UserInterviews(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.profile.is_candidate:
            applications = Application.objects.filter(candidate=request.user).values_list('id')
            interviews = Interview.objects.filter(applications__in=applications)
            serializer = InterviewSerializer(interviews, many=True)
            return JsonResponse(serializer.data, safe=False)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class OpeningApply(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        if not user.profile.is_candidate:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        try:
            opening_id = request.data.get('opening_id')
            opening = Opening.objects.get(id=opening_id)
            eligible_courses = opening.eligible_courses.values_list('id')
            user_course = user.candidate.course
            if (user_course.id,) in list(eligible_courses):
                try:
                    Application.objects.get(opening=opening_id, candidate=user.candidate)
                    return Response({'status': 'already applied', "opening_id": opening_id}, status=status.HTTP_200_OK)
                except Exception as e:
                    Application.objects.create(opening_id=opening_id, candidate=user.candidate)
                    return Response({'status': 'applied'}, status=status.HTTP_200_OK)
            else:
                return Response(
                    {'error': "Couldn't apply to this opening. Branch not allowed"},
                    status=status.HTTP_400_BAD_REQUEST
                )
        except Exception as e:
            return Response({'error': str(e.message)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class InterviewAction(APIView):
    permission_classes = [IsAuthenticated, IsInterviewerOrRecruiter]

    def post(self, request, *arg, **kwargs):
        actions = InterviewActions
        action = self.kwargs.get('action', '')
        print(action)

        if action not in actions.keys():
            return Response({'status': 'Invalid Action'}, status=status.HTTP_400_BAD_REQUEST)

        return actions[action](request)


class Interviewers(APIView):
    permission_classes = [IsAuthenticated, IsInterviewerOrRecruiter]

    def get(self, request):
        company = request.META['COMPANY']
        interviewers = Interviewer.objects.filter(company=company)
        serializer = InterviewerSerializer(interviewers, many=True)
        return JsonResponse(serializer.data, safe=False)

