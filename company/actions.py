from rest_framework.response import Response
from rest_framework import status
from .models import Interview
from datetime import datetime


def create_interview(request):
    data = request.data
    application_id = data.get('application', '')
    interviewer_id = data.get('interviewer')
    candidate_id = data.get('candidate', '')
    scheduled_at = data.get('scheduled_at', '')
    description = data.get('description', '')

    if application_id == '' or candidate_id == '' or scheduled_at == '' or description == '' or interviewer_id == '':
        return Response({'error': 'data is missing'}, status=status.HTTP_400_BAD_REQUEST)
    print("hey", application_id)
    Interview.objects.create(
        interviewer_id=interviewer_id, application_id=application_id, scheduled_at=scheduled_at, description=description
    )
    return Response({'status': 'interview is created'}, status=status.HTTP_200_OK)


def cancel_interview(request):
    data = request.data
    interview_id = data.get('interview', '')

    if interview_id == '':
        return Response({'error': 'data is missing'}, status=status.HTTP_400_BAD_REQUEST)

    interview = Interview.objects.get(id=interview_id)
    interview.cancelled = True

    return Response({'status': 'interview is cancelled'}, status=status.HTTP_200_OK)


InterviewActions = {
    'create': create_interview,
    'cancel': cancel_interview
}