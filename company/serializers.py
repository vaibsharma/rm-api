from rest_framework import serializers
from .models import Company, Recruiter, Opening, Interviewer, Interview
from users.serializers import UserSerializer


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'address', 'mobile']


class RecruiterSerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)

    class Meta:
        model = Recruiter
        fields = ['id', 'company']


class OpeningSerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)
    eligible_courses = serializers.SerializerMethodField()

    class Meta:
        model = Opening
        fields = ['id', 'company', 'university', 'position', 'description', 'cutoff', 'attachment', 'eligible_courses',
                  'created_at']

    def get_eligible_courses(self, obj):
        from university.serializers import CourseSerializer
        courses = CourseSerializer(obj.eligible_courses, many=True)
        return courses.data


class InterviewerSerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)
    user = serializers.SerializerMethodField()

    class Meta:
        model = Interviewer
        fields = ['id', 'company', 'user']

    def get_user(self, obj):
        user = obj.user
        response = dict({})
        response['name'] = "%s %s" % (user.first_name, user.last_name)
        return response



class InterviewSerializer(serializers.ModelSerializer):
    interviewer = serializers.SerializerMethodField()

    class Meta:
        model = Interview
        fields = ['id', 'interviewer', 'description', 'scheduled_at', 'status']

    def get_interviewer(self, obj):
        user_serializer = UserSerializer(obj.interviewer.user)
        return user_serializer.data
