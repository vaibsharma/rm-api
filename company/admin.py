from django.contrib import admin
from .models import Company, Opening, Recruiter, Interview, Interviewer
from django import forms
# Register your models here.


class TestForm(forms.ModelForm):
    class Meta:
        model = Opening
        fields = '__all__'


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    pass


@admin.register(Opening)
class OpeningAdmin(admin.ModelAdmin):
    form = TestForm
    filter_horizontal = ('eligible_courses',)


@admin.register(Recruiter)
class RecruiterAdmin(admin.ModelAdmin):
    pass


@admin.register(Interview)
class InterviewAdmin(admin.ModelAdmin):
    pass


@admin.register(Interviewer)
class InterviewerAdmin(admin.ModelAdmin):
    pass