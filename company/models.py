from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from base_model import RMBase
from utils.commons import phone_regex

# Create your models here.


class Company(RMBase):
    name = models.CharField(max_length=32, blank=False, null=False, unique=True)
    address = models.CharField(max_length=256, blank=True, null=True)
    mobile = models.CharField(validators=[phone_regex], max_length=17, null=True, blank=True)
    avatar = models.ImageField(upload_to='avatar/', blank=True)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return self.name


class Recruiter(RMBase):
    user = models.OneToOneField(User, related_name='recruiter', on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return "%s - %s" % (self.user.username, self.company)


class Opening(RMBase):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    university = models.ForeignKey('university.university', on_delete=models.CASCADE)
    position = models.CharField(max_length=64, blank=False, null=False)
    description = models.TextField(max_length=8096, blank=True)
    attachment = models.FileField(upload_to='job_description')
    cutoff = models.FloatField(default=0)
    eligible_courses = models.ManyToManyField('university.course')

    def __str__(self):
        return "%s - %s - %s" % (self.position, self.company, self.university)


class Interviewer(RMBase):
    user = models.OneToOneField(User, related_name='interviewer', on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.ForeignKey)

    def __str__(self):
        return "%s %s" % (self.user.first_name + " " + self.user.last_name, self.company)


class Interview(RMBase):

    class Status(models.IntegerChoices):
        ONGOING = 0
        REJECT = 1
        BAD = 2
        MAYBE = 3
        GOOD = 4
        EXCELLENT = 5

    interviewer = models.ForeignKey(Interviewer, on_delete=models.CASCADE)
    description = models.CharField(max_length=1024, blank=True)
    application = models.ForeignKey('university.application', related_name='interviews', on_delete=models.CASCADE)
    scheduled_at = models.DateTimeField(null=True)
    status = models.IntegerField(choices=Status.choices, default=Status.ONGOING)
    cancelled = models.BooleanField(default=False)
    review = models.TextField(max_length=2024, null=True)

    def __str__(self):
        return "%s - %s" % (self.application, self.interviewer)



