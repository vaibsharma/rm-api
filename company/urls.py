from django.conf.urls import url, include
import oauth2_provider.views as oauth2_views
from .views import Companies, UserInterviews, OpeningApply, InterviewAction,Interviewers
from django.conf import settings

# OAuth2 provider endpoints

app_name = "university"

urlpatterns = [
    url(r'^companies/$', Companies.as_view(), name='home_feed'),
    url(r'^interviews/$', UserInterviews.as_view(), name='user_interviews'),
    url(r'^interview/(?P<action>[a-zA-Z0-9-_]+)/$', InterviewAction.as_view(), name='interview_actions'),
    url(r'^interviewers/$', Interviewers.as_view(), name='interviewers'),
    url(r'^opening/apply/$', OpeningApply.as_view(), name="apply_opening")
]